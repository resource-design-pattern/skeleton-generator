#!/bin/bash
# @author Exadra37(Paulo Silva) <exadra37ingmailpointcom>
# @since  2016/05/21
# @link   http://exadra37.com

set -e



##################################################################################################################################################################
# Sourcing Dependencies
#########################################d########################################################################################################################

    source dependencies.source.sh
    source functions.source.sh

    vendor_name="exadra37-bash"

    Source_Dependency "${vendor_name}" "file-system" "0.1.0" "sourcing/file-system-trait.source"
    #Source_Dependency "${vendor_name}" "git-helpers" "0.1.0" "sourcing/git-helpers-trait.source"
    #Source_Dependency "${vendor_name}" "pretty-print" "0.1.0" "sourcing/pretty-print-trait.source"
    #Source_Dependency "${vendor_name}" "strings-manipulation" "0.1.0" "sourcing/strings-manipulation-trait.source"
    #Source_Dependency "${vendor_name}" "package-signature-trait" "0.1.0" "sourcing/package-signature-trait.source"

    exit 1


#################################################################################################################################################################
# Read Arguments
#################################################################################################################################################################

    Print_Line "Reading Arguments"

    ([ "${1:0:1}" == "-" ] || [ -z "$1" ]) && Print_Fatal_Error "Missing Package Name, eg: package/name" || package_name="$1"
    ([ "${2:0:1}" == "-" ] || [ -z "$2" ]) && Print_Fatal_Error "Missing file extension for the programming language you want to use, eg: php" || file_extension="$2"
    ([ "${3:0:1}" == "-" ] || [ -z "$3" ]) && Print_Fatal_Error "Missing resource(s), eg: Products or eg: Products,Categories,Users" || resources="$3"

    shift 3

    name_separator="_"

    while getopts ':a:c:e:i:s' flag; do
      case "${flag}" in
        a) appends="$OPTARG";;
        c) commons="$OPTARG";;
        e) excludes="$OPTARG";;
        i) includes="$OPTARG";;
        s) name_separator="";;
        \?) Print_Fatal_Error "option -$OPTARG is not supported.";;
        :) Print_Fatal_Error "option -$OPTARG requires a value.";;
      esac
    done


#################################################################################################################################################################
# Declaring and Assigning Variables
#################################################################################################################################################################

    start_time=`date +%s`
    package="Skeleton Generator"
    version="1.0.0"
    source_code="https://github.com/resource-design-pattern/Skeleton-generator/blob/${version}"
    license="${source_code}/LICENSE"
    docs="${source_code}/README.md"


#################################################################################################################################################################
# Start Execution
#################################################################################################################################################################

    Print_Title ">>> START RESOURCES GENERATOR <<<<"

    vendor_path="$( Get_Vendor_Path ${package_name} )"

    # Generating Main Skeleton
    Generate_Config_Skeleton "${vendor_path}"
    Generate_Deploy_Skeleton "${vendor_path}"
    Generate_Docs_Skeleton "${vendor_path}"
    Generate_Public_Skeleton "${vendor_path}"

    # Generating for Tests
    Generate_Tests_Skeleton "${vendor_path}"
    Generate_Adapters_Skeleton "${vendor_path}/tests/PhpSpec"
    Generate_Factories_Skeleton "${vendor_path}/tests/PhpSpec"
    Generate_Commons_Skeleton  "${vendor_path}/tests/PhpSpec" "${commons}"
    Generate_Resource_Skeleton "${vendor_path}/tests/PhpSpec" "${appends}" "${commons}" "${includes}" "${excludes}" "${package_name}" "${file_extension}" "${resources}" "${name_separator}" "${name_suffix}"

    #Generating for src
    Generate_Adapters_Skeleton "${vendor_path}/src"
    Generate_Factories_Skeleton "${vendor_path}/src"
    Generate_Commons_Skeleton  "${vendor_path}/src" "${commons}"
    Generate_Resource_Skeleton "${vendor_path}/src" "${appends}" "${commons}" "${includes}" "${excludes}" "${package_name}" "${file_extension}" "${resources}" "${name_separator}" "${name_suffix}"


#################################################################################################################################################################
# End Execution
#################################################################################################################################################################

    printPackageInfo "$package" "$version" "$license" "$source_code" "$docs"
    exit 0
